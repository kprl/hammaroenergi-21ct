<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>
</main><!-- #main -->
</div><!-- #primary -->
</div><!-- #content -->

<div class="footer-wrapper">
    <?php get_template_part('images/hammaroenergi', 'illustrationsbild.svg'); ?>

    <?php get_template_part('template-parts/footer/footer-widgets'); ?>

    <footer id="colophon" class="site-footer" role="contentinfo">

        <div class="footer-service">
            <h4>Kontakt vid driftstopp och felanmälan</h4>
            <?php echo hammaroenergi_journummer() . " | " . hammaroenergi_jourepost(); ?>
        </div>

        <?php if (has_nav_menu('footer')) : ?>
            <nav aria-label="<?php esc_attr_e('Secondary menu', 'twentytwentyone'); ?>" class="footer-navigation">
                <ul class="footer-navigation-wrapper">
                    <?php
                    wp_nav_menu(
                        array(
                            'theme_location' => 'footer',
                            'items_wrap'     => '%3$s',
                            'container'      => false,
                            'depth'          => 1,
                            'link_before'    => '<span>',
                            'link_after'     => '</span>',
                            'fallback_cb'    => false,
                        )
                    );
                    ?>
                </ul><!-- .footer-navigation-wrapper -->
            </nav><!-- .footer-navigation -->
        <?php endif; ?>

        <a href="<?php echo esc_url(home_url('/')); ?>">
            <div class="site-logo footer-logo">
                <?php get_template_part('images/hammaroenergi', 'logo.svg'); ?>
            </div>
        </a>
    </footer><!-- #colophon -->
</div>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>