<?php

add_action('acf/init', 'my_acf_blocks_init');
function my_acf_blocks_init()
{

    // Check function exists.
    if (function_exists('acf_register_block_type')) {

        // Register a testimonial block.
        acf_register_block(array(
            'name'            => 'hammaroenergi_journummer',
            'title'           => __('Journummer'),
            'description'     => __('Block för att visa aktuellt journummer'),
            'render_template' => get_stylesheet_directory() . '/blocks/block-hammaroenergi_journummer.php',
            'mode'            => 'preview',
            'icon'            => file_get_contents(get_stylesheet_directory() . '/images/hammaroenergi-symbol.svg'),
            'keywords'        => array('Hammarö Energi', 'jour', 'journummer', 'kprl'),
        ));

        acf_register_block(array(
            'name'            => 'hammaroenergi_personal',
            'title'           => __('Personal'),
            'description'     => __('Block för att visa aktuellt personal'),
            'render_template' => get_stylesheet_directory() . '/blocks/block-hammaroenergi_staff.php',
            'mode'            => 'edit',
            'icon'            => file_get_contents(get_stylesheet_directory() . '/images/hammaroenergi-symbol.svg'),
            'keywords'        => array('Hammarö Energi', 'personal', 'staff', 'kprl'),
        ));

        acf_register_block(array(
            'name'            => 'hammaroenergi_service',
            'title'           => __('Driftinformation'),
            'description'     => __('Block för att visa lista med senaste driftinformationen'),
            'render_template' => get_stylesheet_directory() . '/blocks/block-hammaroenergi_service.php',
            'mode'            => 'preview',
            'icon'            => file_get_contents(get_stylesheet_directory() . '/images/hammaroenergi-symbol.svg'),
            'keywords'        => array('Hammarö Energi', 'driftinformation', 'service', 'kprl'),
        ));

        acf_register_block(array(
            'name'            => 'hammaroenergi_news',
            'title'           => __('Nyheter'),
            'description'     => __('Block för att visa lista med senaste nyheterna'),
            'render_template' => get_stylesheet_directory() . '/blocks/block-hammaroenergi_news.php',
            'mode'            => 'preview',
            'icon'            => file_get_contents(get_stylesheet_directory() . '/images/hammaroenergi-symbol.svg'),
            'keywords'        => array('Hammarö Energi', 'nyheter', 'news', 'kprl'),
        ));

        acf_register_block(array(
            'name'            => 'hammaroenergi_faq',
            'title'           => __('FAQ'),
            'description'     => __('Block för att visa lista med vald FAQ'),
            'render_template' => get_stylesheet_directory() . '/blocks/block-hammaroenergi_faq.php',
            'mode'            => 'edit',
            'icon'            => file_get_contents(get_stylesheet_directory() . '/images/hammaroenergi-symbol.svg'),
            'keywords'        => array('Hammarö Energi', 'frågor', 'svar', 'faq', 'kprl'),
        ));
    }
}

// The code below registers the Custom patterns category and the Author Bio pattern.
// <img src="' . esc_url( get_stylesheet_directory_uri() ) . '/assets/images/Kinsta-logo.png" alt="Kinsta" />
add_action('init', function () {

    register_block_pattern_category(
        'custom-patterns',
        array('label' => esc_html__('Hammarö Energi', 'twentytwentyone'))
    );

    register_block_pattern(
        'hammaroenergi/custom-bio-pattern',
        array(
            'title'         => __('mvh', 'twentytwentyone'),
            'description'   => _x('A block with 2 columns that displays an avatar image, a heading and a paragraph.', 'Block pattern description', 'twentytwentyone'),
            'content'       => '<!-- wp:columns {"verticalAlignment":null} --> <div class="wp-block-columns"><!-- wp:column {"verticalAlignment":"center","width":"33.33%"} --> <div class="wp-block-column is-vertically-aligned-center" style="flex-basis:33.33%"><!-- wp:image {"id":29,"sizeSlug":"large","linkDestination":"none","className":"is-style-rounded"} --> <figure class="wp-block-image size-large is-style-rounded"><img src="' . esc_url(get_stylesheet_directory_uri()) . '/assets/images/Kinsta-logo.png" alt="Kinsta" /></figure> <!-- /wp:image --></div> <!-- /wp:column --> <!-- wp:column {"width":"66.66%"} --> <div class="wp-block-column" style="flex-basis:66.66%"><!-- wp:heading {"level":4} --> <h4>About Kinsta</h4> <!-- /wp:heading --> <!-- wp:paragraph --> <p>Kinsta was founded in 2013 with a desire to change the status quo. We set out to create the best WordPress hosting platform in the world, and that’s our promise. We don’t settle and are here to stay.</p> <!-- /wp:paragraph --></div> <!-- /wp:column --></div> <!-- /wp:columns -->',
            'categories'    => array('hammaroenergi'),
        )
    );

    register_block_pattern(
        'hammaroenergi/jourtelefon',
        array(
            'title'         => __('jourtelefon', 'twentytwentyone'),
            'description'   => _x('Block för att visa jourtelefon för aktuell tidpunkt.', 'Block pattern description', 'twentytwentyone'),
            'content'       => hammaroenergi_journummer(),
            'categories'    => array('hammaroenergi'),
        )
    );
});
