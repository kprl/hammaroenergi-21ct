<?php

/**
 * Block Name: Hammarö Energi - Journummer
 */

$id = 'hammaroenergi_journummer-' . $block['id'];

$align_class  = $block['align'] ? 'align' . $block['align'] : '';
if (array_key_exists('className', $block)) {
    $css_class  = $block['className'];
} else {
    $css_class  = '';
}
?>

<div id="<?php echo $id; ?>" class="hammaroenergi_journummer <?php echo $align_class; ?> <?php echo $css_class; ?>">
    <h4>Kontakt vid driftstopp och felanmälan</h4>
    <p><?php echo hammaroenergi_journummer(); ?></p>
    <p><?php echo hammaroenergi_jourepost(); ?></p>
</div>