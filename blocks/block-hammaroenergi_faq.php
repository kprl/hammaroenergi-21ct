<?php

/**
 * Block Name: Hammarö Energi - FAQ
 */

$ps = null;
$posts = get_field('faq_to_display');
if ($posts) {
    foreach ($posts as $key => $p) {
        $ps[] = $p->ID;
    }
}

$faqArr = return_henergi_faq($ps);
if ($faqArr) :

    $id = 'hammaroenergi_faq-' . $block['id'];

    $align_class  = $block['align'] ? 'align' . $block['align'] : '';
    if (array_key_exists('className', $block)) {
        $css_class  = $block['className'];
    } else {
        $css_class  = '';
    }

?>

    <div id="<?php echo $id; ?>" class="<?php echo $align_class;
                                        echo $css_class; ?>">
        <?php
        foreach ($faqArr as $key => $faq) { ?>

            <div class="hammaroenergi_faq">

                <?php
                if ($key > 0) {
                    echo "<hr />";
                }
                ?>

                <h4><?php echo $faq['title']; ?></h4>

                <?php
                foreach ($faq['faq'] as $theFaq) {
                ?>
                    <details>
                        <summary><?php echo $theFaq['question']; ?></summary>
                        <div class="faq__content">
                            <p><?php echo $theFaq['answer']; ?></p>
                        </div>
                    </details>
                <?php
                }
                ?>

                <?php if (current_user_can('editor') || current_user_can('administrator')) {
                ?>
                    <a class="post-edit-link" href="<?php echo $faq['edit']; ?>">Redigera</a>
                <?php } ?>
            </div>

        <?php } ?>

    </div>

<?php
endif;
