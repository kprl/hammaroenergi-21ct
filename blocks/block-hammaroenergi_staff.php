<?php

/**
 * Block Name: Hammarö Energi - Personal
 */

$ps = null;
$display_options = array('display_image' => 'true', 'display_compact' => 'false', 'num_cols' => 'auto-fit');
$display_options = get_field('display_options');
$posts = get_field('staff_to_display');
if ($posts) {
    foreach ($posts as $key => $p) {
        $ps[] = $p->ID;
    }
}

$personData = return_henergi_staff($ps);
if ($personData) :

    $id = 'hammaroenergi_personal-' . $block['id'];

    $grid = "";
    if ($display_options['num_cols']) $grid = "grid-template-columns: repeat(" . $display_options['num_cols'] . ", minmax(260px, 1fr));";

    $align_class  = $block['align'] ? 'align' . $block['align'] : '';
    if (array_key_exists('className', $block)) {
        $css_class  = $block['className'];
    } else {
        $css_class  = '';
    }

    if ($display_options['display_compact'] === 'true') {
        $css_class  .= 'personal__compact';
    }
?>

    <div id="<?php echo $id; ?>" style="<?php echo $grid; ?>" class="hammaroenergi_personal <?php echo $align_class; ?> <?php echo $css_class; ?>">

        <?php
        foreach ($personData as $key => $pd) { ?>

            <div class="personal__person">
                <div class="personal__content">
                    <h4><?php echo $pd['name']; ?></h4>
                    <div class="personal__title"><?php echo $pd['title']; ?></div>
                    <div class="personal__phone">
                        <?php
                        if ($pd['phone']) {
                            foreach ($pd['phone'] as $key => $phone) {
                                if ($key > 0) echo ", ";
                                echo '<a href="tel:' . $phone['number'] . '">' . $phone['number'] . '</a>';
                            }
                        }
                        ?>
                    </div>
                    <div class="personal__email"><a href="mailto:<?php echo $pd['email']; ?>"><?php echo $pd['email']; ?></a></div>

                    <?php if (current_user_can('editor') || current_user_can('administrator')) {
                    ?>
                        <a class="post-edit-link" href="<?php echo $pd['edit']; ?>">Redigera</a>
                    <?php } ?>

                </div>
                <?php if ($pd['thumbnail'] && $display_options['display_image'] === 'true') { ?>
                    <div class="personal__image">
                        <?php echo $pd['thumbnail']; ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>

<?php

endif;
