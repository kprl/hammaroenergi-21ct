<?php

/**
 * Block Name: Hammarö Energi - Nyheter
 */

$henergiArr = return_henergi_news();
if ($henergiArr) :

    $id = 'hammaroenergi_news-' . $block['id'];

    $align_class  = $block['align'] ? 'align' . $block['align'] : '';
    if (array_key_exists('className', $block)) {
        $css_class  = $block['className'];
    } else {
        $css_class  = '';
    }

?>

    <div id="<?php echo $id; ?>" class="hammaroenergi_news <?php echo $align_class; ?> <?php echo $css_class; ?>">
        <?php
        foreach ($henergiArr as $key => $hes) {
            if ($key % 2) {
                $odd = false;
            } else {
                $odd = true;
            }

        ?>
            <div class="wp-block-media-text is-stacked-on-mobile <?php if (!$odd) echo 'has-media-on-the-right'; ?>">
                <?php if ($hes['thumbnail_id'] === 0) { ?>
                    <figure class="wp-block-media-text__media"><?php echo get_the_post_thumbnail(0); ?></figure>
                <?php } else { ?>
                    <figure class="wp-block-media-text__media"><?php echo get_the_post_thumbnail($hes['ID']); ?></figure>
                <?php } ?>
                <div class="wp-block-media-text__content">
                    <h2><?php echo $hes['title']; ?></h2>
                    <div class="wp-block-media-text__date"><?php echo date("Y-m-d", strtotime($hes['date'])); ?></div>
                    <div class="wp-block-media-text__excerpt"><?php echo get_the_excerpt($hes['ID']); ?></div>
                    <div class="wp-block-media-text__links">
                        <a href="<?php echo $hes['link']; ?>">Läs mer ...</a>
                        <?php if (current_user_can('editor') || current_user_can('administrator')) {
                        ?>
                            <a class="post-edit-link" href="<?php echo $hes['edit']; ?>">Redigera</a>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
<?php

endif;
