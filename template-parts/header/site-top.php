<?php

/**
 * Displays the site navigation.
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>

<?php if (has_nav_menu('footer')) : ?>
    <nav id="top-navigation" class="top-navigation" role="navigation" aria-label="<?php esc_attr_e('Top menu', 'twentytwentyone'); ?>">
        <?php
        wp_nav_menu(
            array(
                'theme_location'  => 'footer',
                'menu_class'      => 'menu-wrapper',
                'container_class' => 'top-menu-container',
                'items_wrap'      => '<ul id="top-menu-list" class="%2$s">%3$s</ul>',
                'fallback_cb'     => false,
            )
        );
        ?>
    </nav><!-- #site-navigation -->
<?php endif; ?>