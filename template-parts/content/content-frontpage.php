<?php

/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php if (has_post_thumbnail()) : ?>
        <header class="entry-header">
            <?php twenty_twenty_one_post_thumbnail(); ?>
            <div class="frontpage-header-text">
                <?php
                $siteInfo = get_field('frontpage', 'option');
                ?>
                <h1><?php echo $siteInfo['title']; ?></h1>
                <p><?php echo $siteInfo['lead']; ?></p>
                <?php if ($siteInfo['button']) { ?>
                    <div class="wp-block-button is-style-fill">
                        <a href="<?php echo $siteInfo['button']['url']; ?>" target="<?php echo $siteInfo['button']['target']; ?>" class="wp-block-button__link button has-text-color has-background">
                            <?php echo $siteInfo['button']['title']; ?>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </header><!-- .entry-header -->
    <?php endif; ?>

    <div class="entry-content">
        <?php
        the_content();

        wp_link_pages(
            array(
                'before'   => '<nav class="page-links" aria-label="' . esc_attr__('Page', 'twentytwentyone') . '">',
                'after'    => '</nav>',
                /* translators: %: Page number. */
                'pagelink' => esc_html__('Page %', 'twentytwentyone'),
            )
        );
        ?>
    </div><!-- .entry-content -->

    <?php if (get_edit_post_link()) : ?>
        <footer class="entry-footer default-max-width">
            <?php
            edit_post_link(
                sprintf(
                    /* translators: %s: Name of current post. Only visible to screen readers. */
                    esc_html__('Edit %s', 'twentytwentyone'),
                    '<span class="screen-reader-text">' . get_the_title() . '</span>'
                ),
                '<span class="edit-link">',
                '</span>'
            );
            ?>
        </footer><!-- .entry-footer -->
    <?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->