<?php

if (!function_exists('cpt_henergi_service')) {

    // Register Custom Post Type
    function cpt_henergi_service()
    {

        $labels = array(
            'name'                  => _x('Driftinformation', 'Post Type General Name', 'twentytwentyone'),
            'singular_name'         => _x('Driftinformation', 'Post Type Singular Name', 'twentytwentyone'),
            'menu_name'             => __('Driftinformation', 'twentytwentyone'),
            'name_admin_bar'        => __('Driftinformation', 'twentytwentyone'),
        );

        $rewrite = array(
            'slug'                  => 'driftinformation',
            'with_front'            => true,
            'pages'                 => false,
            'feeds'                 => true,
        );

        $args = array(
            'label'                 => __('Driftinformation', 'twentytwentyone'),
            'description'           => __('Hammarö Energi - Driftinformation', 'twentytwentyone'),
            'labels'                => $labels,
            'supports'              => array('title', 'editor', 'thumbnail'),
            'taxonomies'            => array('category', 'post_tag'),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
            'show_in_rest'          => true,
        );
        register_post_type('henergi_service', $args);
    }
    add_action('init', 'cpt_henergi_service', 0);
}
