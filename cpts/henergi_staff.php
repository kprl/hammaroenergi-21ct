<?php

if (!function_exists('cpt_henergi_staff')) {

    // Register Custom Post Type
    function cpt_henergi_staff()
    {

        $labels = array(
            'name'                  => _x('Personal', 'Post Type General Name', 'twentytwentyone'),
            'singular_name'         => _x('Personal', 'Post Type Singular Name', 'twentytwentyone'),
            'menu_name'             => __('Personal', 'twentytwentyone'),
            'name_admin_bar'        => __('Personal', 'twentytwentyone'),
        );

        $rewrite = array(
            'slug'                  => 'staff',
            'with_front'            => true,
            'pages'                 => false,
            'feeds'                 => true,
        );

        $args = array(
            'label'                 => __('Personal', 'twentytwentyone'),
            'description'           => __('Hammarö Energi - Personal', 'twentytwentyone'),
            'labels'                => $labels,
            'supports'              => array('title', 'thumbnail'),
            'taxonomies'            => array(),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
            'show_in_rest'          => true,
        );
        register_post_type('henergi_staff', $args);
    }
    add_action('init', 'cpt_henergi_staff', 0);
}
