<?php

if (!function_exists('cpt_henergi_faq')) {

    // Register Custom Post Type
    function cpt_henergi_faq()
    {

        $labels = array(
            'name'                  => _x('FAQ', 'Post Type General Name', 'twentytwentyone'),
            'singular_name'         => _x('FAQ', 'Post Type Singular Name', 'twentytwentyone'),
            'menu_name'             => __('FAQ', 'twentytwentyone'),
            'name_admin_bar'        => __('FAQ', 'twentytwentyone'),
        );

        $rewrite = array(
            'slug'                  => 'faq',
            'with_front'            => true,
            'pages'                 => false,
            'feeds'                 => true,
        );

        $args = array(
            'label'                 => __('Driftinformation', 'twentytwentyone'),
            'description'           => __('Hammarö Energi - Driftinformation', 'twentytwentyone'),
            'labels'                => $labels,
            'supports'              => array('title', 'thumbnail'),
            'taxonomies'            => array(),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
            'show_in_rest'          => true,
        );
        register_post_type('henergi_faq', $args);
    }
    add_action('init', 'cpt_henergi_faq', 0);
}
