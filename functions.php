<?php

date_default_timezone_set('Europe/Stockholm');
setlocale(LC_TIME, 'sv_SE');

/* enqueue scripts and style from parent theme */
function twentytwentyone_styles()
{
    wp_enqueue_style(
        'child-style',
        get_stylesheet_uri(),
        array('twenty-twenty-one-style'),
        wp_get_theme()->get('Version')
    );
}
add_action('wp_enqueue_scripts', 'twentytwentyone_styles');

if (!function_exists('pre_r')) {
    function pre_r($val, $die = 0)
    {
        echo "<pre>";
        print_r($val);
        echo "</pre>";
        if ($die == 1) {
            die();
        }
    }
}

add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts()
{
    echo '<style>
    .toplevel_page_hammaroenergi-options {background-color: #227c6e !important;}
    .toplevel_page_hammaroenergi-options img {height: 15px;}
    </style>';
}

/**
 *
 * Enqueue stylesheet to Block Editor as well
 * (good for child themes where you're just adding the palette CSS)
 * Otherwise enqueue a different sheet for the Block Editor
 *
 */
function add_block_editor_stylesheet()
{
    // Add support for editor styles.
    add_theme_support('editor-styles');

    // Enqueue editor styles.
    add_editor_style('editor.min.css');
}
add_action('after_setup_theme', 'add_block_editor_stylesheet');

// För en gemensam menykategori i adminlägets vänsterspalt.
function hammaroenergi_add_admin_menu()
{

    add_menu_page(
        __('Hammarö Energi', 'twentytwentyone'),
        __('Hammarö Energi', 'twentytwentyone'),
        'acf-options-installningar',
        'hammaroenergi-options',
        '',
        get_stylesheet_directory_uri() . '/images/hammaroenergi-symbol-white.svg',
        3
    );

    add_submenu_page('hammaroenergi-options', __('Hammarö Energi', 'twentytwentyone'), 'FAQ', 'edit_pages', 'edit.php?post_type=henergi_faq');
    remove_menu_page('edit.php?post_type=henergi_faq');
    add_submenu_page('hammaroenergi-options', __('Hammarö Energi', 'twentytwentyone'), 'Personal', 'edit_pages', 'edit.php?post_type=henergi_staff');
    remove_menu_page('edit.php?post_type=henergi_staff');
    add_submenu_page('hammaroenergi-options', __('Hammarö Energi', 'twentytwentyone'), 'Driftinformation', 'edit_pages', 'edit.php?post_type=henergi_service');
    remove_menu_page('edit.php?post_type=henergi_service');
}
add_action('admin_menu', 'hammaroenergi_add_admin_menu');

if (
    function_exists('acf_add_options_page')
) {

    //Inställningar och information
    acf_add_options_sub_page(array(
        'page_title'    => 'Inställningar',
        'menu_title'    => 'Inställningar',
        'parent_slug'   => 'hammaroenergi-options',
        'position'      => 0
    ));
}

// Including CPT:s
require_once('cpts/henergi_service.php');
require_once('cpts/henergi_staff.php');
require_once('cpts/henergi_faq.php');

function hammaroenergi_jourepost($info = true, $link = true)
{
    $epostadress = get_field('epostadress', 'option');
    $return = "";

    if ($epostadress) {
        if ($info) $return .= $epostadress['information'];
        if ($link) {
            $return .= '<a href="mailto:' . $epostadress['epostadress'] . '">' . $epostadress['epostadress'] . '</a>';
        } else {
            $return .= $epostadress['epostadress'];
        }
    }

    return $return;
}

function hammaroenergi_journummer($info = true, $link = true)
{

    $journummer = get_field('journummer', 'option');
    $return = "";

    $dayNow = date("N");
    // $dayNow = 6;
    $dateNow = date('Y-m-d');
    $timeNow = date("H:i");
    // $timeNow = "07:01";
    $timestamp = strtotime($timeNow);

    if ($journummer) {
        foreach ($journummer as $jn) {

            if (in_array($dayNow, $jn['dagar'])) {
                if (strtotime($jn['klockslag_from']) == strtotime($jn['klockslag_to'])) {
                    $dateNow = date('Y-m-d', strtotime(' +1 day'));
                } else if (strtotime($jn['klockslag_from']) > strtotime($jn['klockslag_to'])) {
                    $dateNow = date('Y-m-d', strtotime(' +1 day'));
                }

                if ($timestamp >= strtotime($jn['klockslag_from']) and $timestamp <= strtotime($dateNow . " " . $jn['klockslag_to'])) {
                    if ($info) $return .= $jn['information'];
                    if ($link) {
                        $return .= '<a href="tel:' . $jn['telefonnummer'] . '">' . $jn['telefonnummer'] . '</a>';
                    } else {
                        $return .= $jn['telefonnummer'];
                    }
                }
            }
        }
    }
    return $return;
}

require_once('block_functions.php');

function return_henergi_service($antal = null, $orderby = "id", $order = "DESC")
{

    $arr = array();

    if (!$antal) {
        $antal = -1;
    }

    $hesArr = get_posts(array(
        'post_type'         => 'henergi_service',
        'post__in'          => $arr,
        'posts_per_page'    => $antal,
        'order'             => $order,
        'orderby'           => $orderby,
        'status'            => 'published',
    ));

    $henergiArr = array();

    foreach ($hesArr as $key => $hes) {
        $henergiArr[$key]['ID']            = $hes->ID;
        $henergiArr[$key]['title']         = $hes->post_title;
        $henergiArr[$key]['date']          = $hes->post_date;
        $henergiArr[$key]['content']       = wpautop($hes->post_content);
        $henergiArr[$key]['excerpt']       = wpautop($hes->post_content);
        $henergiArr[$key]['thumbnail_id']  = get_post_thumbnail_id($hes->ID);
        $henergiArr[$key]['link']          = get_post_permalink($hes->ID);
        $henergiArr[$key]['edit']          = get_edit_post_link($hes->ID);

        $fields = get_fields($hes->ID);

        // if ($fields['utdrag']) {
        //     $henergiArr[$key]['excerpt']     = $fields['utdrag'];
        // }

        $fields = array();
    }

    return $henergiArr;
}

function return_henergi_news($antal = null, $orderby = "id", $order = "DESC")
{

    if (!$antal) {
        $antal = -1;
    }

    $hesArr = get_posts(array(
        'category'          => 5,
        'posts_per_page'    => $antal,
        'order'             => $order,
        'orderby'           => $orderby,
        'status'            => 'published',
    ));

    $henergiArr = array();

    foreach ($hesArr as $key => $hes) {
        $henergiArr[$key]['ID']            = $hes->ID;
        $henergiArr[$key]['title']         = $hes->post_title;
        $henergiArr[$key]['date']          = $hes->post_date;
        $henergiArr[$key]['content']       = wpautop($hes->post_content);
        $henergiArr[$key]['excerpt']       = wpautop($hes->post_content);
        $henergiArr[$key]['thumbnail_id']  = get_post_thumbnail_id($hes->ID);
        $henergiArr[$key]['link']          = get_post_permalink($hes->ID);
        $henergiArr[$key]['edit']          = get_edit_post_link($hes->ID);

        $fields = get_fields($hes->ID);

        $fields = array();
    }

    return $henergiArr;
}

function return_henergi_faq($posts = array(), $antal = null)
{

    if (!$antal) $antal = -1;

    $hesArr = get_posts(array(
        'post_type'         => 'henergi_faq',
        'include'           => $posts,
        'posts_per_page'    => $antal,
        'status'            => 'published',
        'orderby'           => 'post__in',
    ));

    $henergiArr = array();

    foreach ($hesArr as $key => $hes) {
        $henergiArr[$key]['ID']            = $hes->ID;
        $henergiArr[$key]['title']         = $hes->post_title;
        $henergiArr[$key]['link']          = get_post_permalink($hes->ID);
        $henergiArr[$key]['edit']          = get_edit_post_link($hes->ID);

        $henergiArr[$key]['faq']           = array();
        $fields = get_fields($hes->ID);
        if ($fields['faq']) {
            $henergiArr[$key]['faq']       = $fields['faq'];
        }
        $fields = array();
    }

    return $henergiArr;
}

function return_henergi_staff($posts = array(), $antal = null)
{

    if (!$antal) $antal = -1;

    $hesArr = get_posts(array(
        'post_type'         => 'henergi_staff',
        'include'           => $posts,
        'posts_per_page'    => $antal,
        'status'            => 'published',
        'orderby'           => 'post__in',
    ));

    $henergiArr = array();

    foreach ($hesArr as $key => $hes) {
        $henergiArr[$key]['ID']            = $hes->ID;
        $henergiArr[$key]['name']          = $hes->post_title;
        $henergiArr[$key]['thumbnail']     = get_the_post_thumbnail($hes->ID, 'medium');
        $henergiArr[$key]['link']          = get_post_permalink($hes->ID);
        $henergiArr[$key]['edit']          = get_edit_post_link($hes->ID);

        $henergiArr[$key]['title']         = "";
        $henergiArr[$key]['email']         = "";
        $henergiArr[$key]['phone']         = array();
        $fields = get_fields($hes->ID);

        if ($fields['title']) $henergiArr[$key]['title'] = $fields['title'];
        if ($fields['email']) $henergiArr[$key]['email'] = $fields['email'];
        if ($fields['phone']) $henergiArr[$key]['phone'] = $fields['phone'];

        $fields = array();
    }

    return $henergiArr;
}
